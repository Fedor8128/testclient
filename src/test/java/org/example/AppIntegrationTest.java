package org.example;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.jupiter.api.Tag;

/**
 * Unit test for simple App.
 */
public class AppIntegrationTest
{
    /**
     * Rigorous Test :-)
     */
    @Test
    @Tag("integration-test")
    public void MultiClientTest(){
        String input = "Hallo";
        SendingClient client = new SendingClient();
        client.startConnection("localhost", 5000);
        String response = client.sendMessage(input);
        assertEquals((input + " + 2e client"), response);




    }
}
