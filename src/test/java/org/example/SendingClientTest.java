package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SendingClientTest {

    @Test
    void testableMethod() {
        SendingClient sendingClient = new SendingClient();
        String input = "test";
        assertEquals(input, sendingClient.testableMethod(input));

    }
}