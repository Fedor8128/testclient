package org.example;

import java.io.*;
import java.net.*;

public class SendingClient {

    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;

    public void startConnection(String ip, int port) {
        try {
            clientSocket = new Socket(ip, port);
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        } catch (IOException e) {
        }

    }

    public String testableMethod(String input){
        return input;
    }

    public String sendMessage(String msg) {
        try {
            out.println(msg);
            var line = in.readLine();
            return line;
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    public void stopConnection() {
        try {
            in.close();
            out.close();
            clientSocket.close();
        } catch (IOException e) {
//            LOG.debug("error when closing", e);
        }

    }

    public static void main(String[] args) {
        SendingClient client = new SendingClient();
        client.startConnection("localhost", 5000);
        String response = client.sendMessage("test");
        System.out.println(response);

    }
}
